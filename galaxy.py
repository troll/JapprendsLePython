import turtle
from turtle import *
from random import *

# taille de la fenêtre
largeur = 500
hauteur = 500
setup(largeur, hauteur)
# mode couleur
colormode(255)
# vitesse
speed(0)
# tirage au sort du nombre de formes a dessiner
nbre_formes = 1000
#taille du stylo
pensize(1)

bgcolor("#000")
title("galaxy")

def pick_color():
    colors = ["#77a5d9","#eaa230","white","#f5d6bb"]
    j = (len(colors) - 1)
    i = randint(0,j)
    return colors[i]


for k in range(nbre_formes):
    r = uniform(0.1, 2.0)
    begin_fill()
    circle(r) 
    #color=("white")
    color= pick_color()
    fillcolor(color)
    end_fill()
    # coordonnées de départ
    nbreX = randint(-largeur/2, largeur/2)
    nbreY = randint(-hauteur/2, hauteur/2)
    penup()
    goto(nbreX, nbreY)
    pendown()


cv=getcanvas()
cv.postscript(file="file_name.ps", colormode='color')
done()


exitonclick()
