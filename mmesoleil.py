#Importer 
from random import *
from colorama import *


compteur_reussite = 0
jouer_encore = 1

while jouer_encore == 1:
    chiffre_a_deviner = randint(1,6)
    # nbre de numéros a deviner
    n = 6
    reponse_joueur = input(Fore.YELLOW +"\n Devinez quel chiffre a été tiré au sort entre 1 et 6 : ")
    reponse_joueur = int(reponse_joueur)

    if reponse_joueur == chiffre_a_deviner:
        print(Fore.GREEN +"\n >>> Bravo c'est excellent, tu as réellement un don de voyance, c'est dingue!")
        compteur_reussite += 1
        print("\n Tu as deviné {} fois de suite la bonne réponse!".format(compteur_reussite ))
        chances = (1 / n ** compteur_reussite *100 )
        print("\n Tes chances étaient de {} pourcent".format(1 / n ** compteur_reussite *100 ))

    else:
        print(Fore.RED +">>> Désolé, ce n'est pas la bonne réponse! Tu n'es pas Mme Soleil :( ")
        compteur_reussite = 0
        i = input(Fore.WHITE +"\n Tapez *0 pour quitter, *1 pour continuer : " )
        jouer_encore = int(i)
        

